import NotesList from '../views/NotesList.vue'
import SingleNote from '../views/SingleNote.vue'

export const routes = [
  {
    path: '/',
    name: 'NotesList',
    component: NotesList,
  },
  {
    path: '/:id',
    name: 'SingleNote',
    component: SingleNote,
  },
  {
    path: '*',
    redirect: '/'
  },
]
