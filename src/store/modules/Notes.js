export default {
  state: {
    notes: [
      {
        id: '1',
        name: 'Note 1',
        todos: [
          {
            id: '1',
            isDone: false,
            text: 'Todo #1',
          },
          {
            id: '2',
            isDone: true,
            text: 'Todo #2',
          },
          {
            id: '3',
            isDone: false,
            text: 'Todo #3',
          },
          {
            id: '4',
            isDone: false,
            text: 'Todo #3',
          },
          {
            id: '5',
            isDone: false,
            text: 'Todo #3',
          },
        ],
      },
      {
        id: '2',
        name: 'Note 2',
        todos: [
          {
            id: '1',
            isDone: false,
            text: 'Todo_Note_2 #1',
          },
          {
            id: '2',
            isDone: false,
            text: 'Todo_Note_2 #2',
          },
          {
            id: '3',
            isDone: false,
            text: 'Todo_Note_2 #3',
          },
        ],
      },
      {
        id: '3',
        name: 'Note 3',
        todos: [
          {
            id: '1',
            isDone: false,
            text: 'Todo_Note_3 #1',
          },
          {
            id: '2',
            isDone: false,
            text: 'Todo_Note_3 #2',
          },
          {
            id: '3',
            isDone: false,
            text: 'Todo_Note_3 #3',
          },
        ],
      },
    ],
  },
  actions: {
    getNotes({ commit }) {
      const notes = localStorage.getItem('notes')
      if (notes) {
        commit('UPDATE_NOTES', JSON.parse(notes))
      }
    },
    addNote({ commit, state }, note = null) {
      const newNotes = [...state.notes, note]
      commit('UPDATE_NOTES', newNotes)
    },
    editNote({ commit, state }, note = null) {
      const newNotes = state.notes.map(n => (note.id === n.id ? note : n))
      commit('UPDATE_NOTES', newNotes)
    },
    removeNote({ commit, state }, id = null) {
      const newNotes = state.notes.filter(el => el.id !== id)
      commit('UPDATE_NOTES', newNotes)
    },
  },
  mutations: {
    UPDATE_NOTES(state, notes = []) {
      localStorage.setItem('notes', JSON.stringify(notes))
      state.notes = notes
    },
  },
  getters: {
    notesList: state => state.notes,
  },
}
